package iplocation_test

import (
	"net"
	"reflect"
	"testing"

	"gitlab.com/iadgarov/rate-limited-ip-location-ex/app/iplocation"
	"gitlab.com/iadgarov/rate-limited-ip-location-ex/mocks"

	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"
)

func TestService_IPLocation(t *testing.T) {
	type mockData struct {
		times int
		loc   iplocation.Location
		err   error
	}

	genericError := errors.New("some error")

	tests := []struct {
		name      string
		md        mockData
		ip        string
		want      iplocation.Location
		wantErr   bool
		wantedErr error
	}{
		{
			name: "valid input ip expect valid location output",
			ip:   "1.1.1.1",
			md: mockData{
				times: 1,
				loc:   iplocation.Location{City: "some city", Country: "some country"},
			},
			want: iplocation.Location{City: "some city", Country: "some country"},
		},
		{
			name:      "invalid input ip expect invalid input error",
			ip:        "not an ip",
			wantErr:   true,
			wantedErr: iplocation.InvalidIP,
		},
		{
			name: "repository call failure expect error",
			ip:   "1.1.1.1",
			md: mockData{
				times: 1,
				err:   genericError,
			},
			wantErr:   true,
			wantedErr: genericError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := gomock.NewController(t)
			defer c.Finish()
			mRepo := mocks.NewMockRepository(c)
			mRepo.EXPECT().IPLocation(gomock.AssignableToTypeOf(net.IP{})).Times(tt.md.times).Return(tt.md.loc, tt.md.err)

			s := iplocation.NewIPLocationService(mRepo)

			got, err := s.IPLocation(tt.ip)
			if (err != nil) != tt.wantErr {
				t.Fatalf("Service.IPLocation() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.wantErr && !errors.Is(err, tt.wantedErr) {
				t.Fatalf("Service.IPLocation() error = %v, wantedErr %v", err, tt.wantedErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Fatalf("Service.IPLocation() = %v, want %v", got, tt.want)
			}
		})
	}
}
