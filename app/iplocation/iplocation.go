package iplocation

import (
	"log"
	"net"

	"github.com/pkg/errors"
)

var InvalidIP = errors.New("Invalid input ip specified")

// Repository exposes 'get by language' operations for the 'hello world' message
// mockgen -source ./app/iplocation/iplocation.go -destination ./mocks/repository_mock.go -mock_names Repository=MockRepository -package mocks Repository
type Repository interface {
	IPLocation(ip net.IP) (Location, error)
	Teardown() error
}

type Location struct {
	Country string `json:"country"`
	City    string `json:"city"`
}

// Service defines the hello world service
type Service struct {
	repo Repository
}

// NewIPLocationService is the provider for the hello world domain
func NewIPLocationService(repo Repository) *Service {
	log.Println("Creating new IP Location Service...")
	return &Service{
		repo: repo,
	}
}

// IPLocation accepts a string representing an IP and returns its location
func (s *Service) IPLocation(ip string) (Location, error) {
	log.Printf("Parsing input ip value (%s)", ip)
	pip := net.ParseIP(ip)
	if pip == nil {
		return Location{}, InvalidIP
	}

	log.Printf("Getting location of %s from repository", ip)
	l, err := s.repo.IPLocation(pip)
	if err != nil {
		return Location{}, errors.Wrap(err, "repository call failed")
	}
	// if this were a proper service the domain would probably do something clever with the data from the repo right about here

	return l, nil
}

func (s *Service) Teardown() error {
	return s.repo.Teardown()
}
