package http

import (
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

func (d *Driver) routes(timeout time.Duration, rps int) *chi.Mux {
	log.Println("Defining HTTP routes...")
	router := chi.NewRouter()

	router.Use(middleware.Timeout(timeout))
	router.Use(middleware.Logger)
	router.Use(d.rateLimiter)
	router.Route("/v1/find-country", func(r chi.Router) {
		r.Get("/", d.ipLocation)
	})
	return router
}

func (d *Driver) rateLimiter(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if err := d.limiter.Consume(); err != nil {
			// rate limit reached
			writeError(w, http.StatusTooManyRequests, err)
			return
		}
		next.ServeHTTP(w, r)
	})
}
