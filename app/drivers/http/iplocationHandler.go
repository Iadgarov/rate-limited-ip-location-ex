package http

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/iadgarov/rate-limited-ip-location-ex/app/iplocation"

	"github.com/pkg/errors"
)

const (
	queryKeyIP = "ip"
)

// HelloWorld handles HTTP GET requests to get a 'hello world' message in the chosen language
func (d *Driver) ipLocation(w http.ResponseWriter, r *http.Request) {
	ip, ok := r.URL.Query()[queryKeyIP]
	if !ok {
		err := errors.New("Missing IP query")
		writeError(w, http.StatusBadRequest, err)
		return
	}
	if len(ip) != 1 {
		err := errors.Errorf("Invalid IP query value (%s)", ip)
		writeError(w, http.StatusBadRequest, err)
		return
	}

	ipLoc, err := d.ipSrv.IPLocation(ip[0])
	if err != nil {
		if errors.Is(err, iplocation.InvalidIP) {
			writeError(w, http.StatusBadRequest, err)
			return
		}
		err = errors.Wrapf(err, "query (%s=%s) failed", queryKeyIP, ip)
		writeError(w, http.StatusInternalServerError, err)
		return
	}

	res, err := json.Marshal(ipLoc)
	if err != nil {
		err = errors.Wrapf(err, "failed to marshal query result (%s)", string(res))
		writeError(w, http.StatusInternalServerError, err)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	writeBody(w, res)
	return
}

func generateHTTPError(err error) ([]byte, error) {
	b := struct {
		Err string `json:"error"`
	}{
		Err: err.Error(),
	}
	return json.Marshal(b)
}

func writeError(w http.ResponseWriter, status int, err error) {
	log.Printf("Returning status (%v) error message: %s", status, err)
	b, err := generateHTTPError(err)
	if err != nil {
		writeFail(w)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	writeBody(w, b)
}

func writeBody(w http.ResponseWriter, b []byte) {
	if _, err := w.Write(b); err != nil {
		writeFail(w)
		return
	}
}

func writeFail(w http.ResponseWriter) {
	log.Println("Failed to write HTTP response")
	w.WriteHeader(http.StatusInternalServerError)
	return
}
