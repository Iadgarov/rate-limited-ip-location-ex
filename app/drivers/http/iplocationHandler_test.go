package http

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/iadgarov/rate-limited-ip-location-ex/app/iplocation"
	"gitlab.com/iadgarov/rate-limited-ip-location-ex/mocks"
	"gitlab.com/iadgarov/rate-limited-ip-location-ex/pkg/ratelimit"

	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"
)

func TestDriver_ipLocation(t *testing.T) {

	type mockdData struct {
		times    int
		wantedIP string
		err      error
		loc      iplocation.Location
	}

	validLocation := iplocation.Location{Country: "some country", City: "some city"}
	validQuery := map[string]string{"ip": "1.1.1.1"}
	invalidQuery := map[string]string{"ip": "fake"}

	tests := []struct {
		name         string
		query        map[string]string
		wantedStatus int
		md           mockdData
	}{
		{
			name:  "valid query expect valid output",
			query: validQuery,
			md: mockdData{
				times:    1,
				loc:      validLocation,
				wantedIP: "1.1.1.1",
			},
			wantedStatus: http.StatusOK,
		},
		{
			name:         "no query expect bad request (400)",
			wantedStatus: http.StatusBadRequest,
		},
		{
			name:  "ip location domain internal error expect internal server error (500)",
			query: validQuery,
			md: mockdData{
				times:    1,
				err:      errors.New("some error"),
				wantedIP: "1.1.1.1",
			},
			wantedStatus: http.StatusInternalServerError,
		},
		{
			name:  "invalid ip input expect bad request (400)",
			query: invalidQuery,
			md: mockdData{
				times:    1,
				err:      iplocation.InvalidIP,
				wantedIP: "fake",
			},
			wantedStatus: http.StatusBadRequest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			c := gomock.NewController(t)
			defer c.Finish()
			dm := mocks.NewMockIPLocationSrv(c)
			dm.EXPECT().IPLocation(tt.md.wantedIP).Times(tt.md.times).Return(tt.md.loc, tt.md.err)

			d := Driver{
				ipSrv: dm,
			}

			req, err := http.NewRequest(http.MethodGet, "/find-country", nil)
			if err != nil {
				t.Fatal(err)
			}
			q := req.URL.Query()
			for k, v := range tt.query {
				q.Add(k, v)
			}
			req.URL.RawQuery = q.Encode()

			rr := httptest.NewRecorder()
			handler := http.HandlerFunc(d.ipLocation)
			handler.ServeHTTP(rr, req)

			if status := rr.Code; status != tt.wantedStatus {
				t.Errorf("handler returned wrong status code: got %v want %v",
					status, tt.wantedStatus)
			}
		})
	}
}

func TestRateLimit(t *testing.T) {

	validLocation := iplocation.Location{Country: "some country", City: "some city"}
	validQuery := map[string]string{"ip": "1.1.1.1"}

	c := gomock.NewController(t)
	defer c.Finish()
	dm := mocks.NewMockIPLocationSrv(c)
	dm.EXPECT().IPLocation("1.1.1.1").AnyTimes().Return(validLocation, nil)

	d := Driver{
		ipSrv:   dm,
		limiter: ratelimit.NewLimiter(1),
	}

	req, err := http.NewRequest(http.MethodGet, "/find-country", nil)
	if err != nil {
		t.Fatal(err)
	}
	q := req.URL.Query()
	for k, v := range validQuery {
		q.Add(k, v)
	}
	req.URL.RawQuery = q.Encode()

	handler := d.rateLimiter(http.HandlerFunc(d.ipLocation)) // use rate limiter middleware

	testRequest := func(wantedStatus int) {
		rr := httptest.NewRecorder()
		handler.ServeHTTP(rr, req)
		if status := rr.Code; status != wantedStatus {
			t.Fatalf("handler returned wrong status code: got %v want %v",
				status, wantedStatus)
		}
		t.Log("response as expected", wantedStatus)
	}

	t.Log("Execute two calls with a no rest expect 200OK and then 429StatusTooManyRequests")
	testRequest(http.StatusOK)
	testRequest(http.StatusTooManyRequests)
	t.Log("Sleep for 2 seconds and perform call expect 200OK")
	time.Sleep(time.Second)
	testRequest(http.StatusOK)

}
