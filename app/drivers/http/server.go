package http

import (
	"context"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"gitlab.com/iadgarov/rate-limited-ip-location-ex/app/iplocation"
	"gitlab.com/iadgarov/rate-limited-ip-location-ex/pkg/ratelimit"

	"github.com/pkg/errors"
)

const (
	timeout = 15
	port    = 8080

	envKeyRPS = "REQUESTS_PER_SECOND"
)

// Server is an interface for the golang http pkg. We define this to allow for mocking in tests
type Server interface {
	ListenAndServe() error
	Shutdown(ctx context.Context) error
}

// IPLocation is an interface for the IP Location domain
// mockgen -source ./app/drivers/http/server.go -destination ./mocks/ipLocationDomain__mock.go -mock_names IPLocation=MockIPLocationSrv -package mocks IPLocation
type IPLocation interface {
	IPLocation(ip string) (iplocation.Location, error)
	Teardown() error
}

// Driver defines an HTTP driving adapter
type Driver struct {
	server  Server
	timeout time.Duration
	limiter *ratelimit.Limiter

	ipSrv IPLocation
}

// Start boots up the HTTP server
func (d *Driver) Start() error {
	log.Println("Starting HTTP server...")
	return d.server.ListenAndServe()
}

// Stop handles graceful shutdown of the http driver and related domains
func (d *Driver) Stop(ctx context.Context) []error {
	var errs []error
	log.Println("Tearing down IPLocation domain...")
	if err := d.ipSrv.Teardown(); err != nil {
		errs = append(errs, errors.Wrap(err, "failed to teardown IPLocation domain"))
	}

	log.Println("Shutting down HTTP server...")
	if err := d.server.Shutdown(ctx); err != nil {
		errs = append(errs, errors.Wrap(err, "failed to gracefully shutdown HTTP server"))
	}

	return errs
}

// NewDriver defines a provider for the HTTP driver
func NewDriver(ipSrv IPLocation) (*Driver, error) {
	log.Println("Creating new HTTP driver...")
	d := Driver{
		ipSrv: ipSrv,
	}

	sRPS := os.Getenv(envKeyRPS)
	rps, err := strconv.Atoi(sRPS)
	if err != nil {
		return nil, errors.Wrapf(err, "invalid value for RPS specified %s", sRPS)
	}
	log.Println("Setting RPS limit to", rps)

	d.limiter = ratelimit.NewLimiter(rps)

	d.server = &http.Server{
		Handler: d.routes(timeout*time.Second, rps),
		Addr:    ":" + strconv.Itoa(port),
	}
	return &d, nil
}
