package mmdb

import (
	"net"

	"gitlab.com/iadgarov/rate-limited-ip-location-ex/app/iplocation"

	"github.com/oschwald/geoip2-golang"
	"github.com/pkg/errors"
)

const (
	english = "en"
)

type Adapter struct {
	db *geoip2.Reader
}

func NewAdapter(path string) (*Adapter, error) {
	db, err := geoip2.Open(path)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to open %s", path)
	}
	return &Adapter{
		db: db,
	}, nil
}

func (a *Adapter) IPLocation(ip net.IP) (iplocation.Location, error) {
	record, err := a.db.City(ip)
	if err != nil {
		return iplocation.Location{}, errors.Wrapf(err, "failed to query for IP(%s)", ip)
	}

	return iplocation.Location{
		Country: record.Country.Names[english],
		City:    record.City.Names[english],
	}, nil
}

func (a *Adapter) Teardown() error {
	return a.db.Close()
}
