package mmdb_test

import (
	"net"
	"reflect"
	"testing"

	"gitlab.com/iadgarov/rate-limited-ip-location-ex/app/iplocation"
	"gitlab.com/iadgarov/rate-limited-ip-location-ex/pkg/mmdb"
)

func TestIntegrationAdapter(t *testing.T) {
	a, err := mmdb.NewAdapter("../../GeoLite2-City.mmdb")
	if err != nil {
		t.Fatal("Failed to init adapter")
	}
	testStop := func() {
		if err := a.Teardown(); err != nil {
			t.Fatal("Failed to teardown adapter")
		}
	}

	ip := "46.116.37.104"
	want := iplocation.Location{
		City:    "Herzliya",
		Country: "Israel",
	}
	got, err := a.IPLocation(net.ParseIP(ip))
	if err != nil {
		testStop()
		t.Fatal("Failed to get location of valid ip")
	}
	if !reflect.DeepEqual(got, want) {
		testStop()
		t.Fatalf("Did not get expected results.\nGot:  %+v\nWant: %+v", got, want)
	}
	testStop()
}
