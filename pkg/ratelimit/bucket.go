package ratelimit

import (
	"sync/atomic"
	"time"

	"github.com/pkg/errors"
)

type Limiter struct {
	bkt chan time.Time
	ops *int32
}

// NewLimiter provides a burst rate limiter
func NewLimiter(rps int) *Limiter {
	bkt := make(chan time.Time, rps)
	for i := 0; i < rps; i++ {
		bkt <- time.Now()
	}

	var ops int32
	atomic.AddInt32(&ops, int32(rps))
	l := Limiter{
		bkt: bkt,
		ops: &ops,
	}

	go func() {
		// NOTE: time.Tick leaks, in a proper service we would use a Ticker and a closing channel
		period := time.Duration(int64(time.Second) / int64(rps))
		for t := range time.Tick(period) {
			bkt <- t
		}
	}()
	return &l
}

func (l *Limiter) Consume() error {
	select {
	case <-l.bkt:
		return nil
	default:
		return errors.New("Rate limit reached")
	}
}
