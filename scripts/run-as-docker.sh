#!/usr/bin/env bash
docker build -t  iadgarov/rate-limited-ip-location-ex -f build/package/Dockerfile .
docker run -p 8080:8080 -e DATA_SOURCE=mmdb -e  REQUESTS_PER_SECOND=5 -d --name rate-limited-ip-location-ex iadgarov/rate-limited-ip-location-ex