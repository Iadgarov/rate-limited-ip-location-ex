# Rate Limited IP Location EX

A small microservice implementing an IP GeoLocation HTTP endpoint with basic rate limiting.

## Running the service
### Environment variables
The service requires two environment variables to be set in order to run. Otherwise boot will fail. No default values were given.

|   |   |   | 
|---|---|---|
| `DATA_SOURCE`   | IP Location database  |  Supported values: `mmdb` |   
| `REQUESTS_PER_SECOND`  |  Rate limit in requests per second | Supported values: any positive, whole, number  |  

### Running as a container

Docker build: `docker build -t  iadgarov/rate-limited-ip-location-ex -f build/package/Dockerfile .`

For your convenience a Docker build and run script has been provided in: `scripts/run-as-docker.sh`

The script runs the container on port `8080`, with the `Maxmind GeoLite2-City` database and a rate limit of `5` requests per second.

## Architecture

The code is written in a  basic Hexagonal architecture. 
Containing:

|   |   |
|---|---|
| Driving adapter  |   HTTP server|
| Domain| IP location domain|
| Driven adapter| Maxmind GeoLite2 MMDB adapter| 

Every component interacts with the other via an Interface and with the help of dependency injection.
As a result, as long as the interface is maintained, any component can be replaced with a different implementation.

In addition the code contains a package for very basic token bucket rate limiting. 
Applied as part of a middleware in the HTTP router.


## GeoLocation Data Sources
The service support one IP location source, the `Maxmind GeoLite2` database. [Details](https://dev.maxmind.com/geoip/geoip2/geolite2/).

Additional sources may be added by creation of new driven adapters, for instance an adapter to a CSV file. 
These new adapters can then be injected into the existing domain. 

## Code structure
The code follows the common convention for `golang` services, depicted [here](https://github.com/golang-standards/project-layout).

### In detail:
```$xslt
rate-limited-ip-location-ex-master/
├── app                                     // Application code
│   ├── app.go                              // application layer, gluing components together
│   ├── drivers                         
│   │   └── http                            // HTTP driving adapter
│   │       ├── iplocationHandler.go        // handlers
│   │       ├── iplocationHandler_test.go
│   │       ├── routers.go                  // HTTP routes and middleware
│   │       └── server.go                   // Adapter definitions
│   └── iplocation                          // IP location domain
│       ├── iplocation.go           
│       └── iplocation_test.go
├── build
│   ├── ci
│   │   └── .gitlab-ci.yaml                 // Very basic GitLab CI, because why not                                  
│   └── package
│       └── Dockerfile                      // Two stage docker file
├── cmd
│   └── server
│       └── main.go                         // Entry point to code, executes injection and runs
├── GeoLite2-City.mmdb                      // Maxmind GeoLite2-City DB
├── Go Language Project.pdf                 // Exercise details 
├── go.mod
├── go.sum
├── LICENSE
├── mocks                                   // Mockgen mocks for unit tests
│   ├── ipLocationDomain__mock.go
│   └── repository_mock.go
├── pkg                                     // Code used by application
│   ├── mmdb                                // GeoLite2 MMDB driven adapter
│   │   ├── mmdb.go
│   │   └── mmdb_test.go
│   └── ratelimit
│       └── bucket.go                       // Basic rate limiting lib
├── README.md
├── scripts
│   └── run-as-docker.sh                    // Docker build + run with default configs for your convenience  
└── wire
    ├── wire_gen.go                         // Dependency injection with wire (https://github.com/google/wire)
    └── wire.go             
```

## Testing

The code contains a small amount of unit-tests for its various components, each covering most of the "interesting" functions.
The tests are written in a mix of `table-driven` and regular style. 

The driven adapter for the GeoLite2 DB serves as an integration test as well, using a real database and comparing to a real result.

## Rate Limiting

A very basic [token bucket](https://en.wikipedia.org/wiki/Token_bucket) implementation using golang channels. 