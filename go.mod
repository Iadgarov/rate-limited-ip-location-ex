module gitlab.com/iadgarov/rate-limited-ip-location-ex

go 1.15

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/golang/mock v1.4.4
	github.com/google/wire v0.4.0
	github.com/oschwald/geoip2-golang v1.4.0
	github.com/oschwald/maxminddb-golang v1.7.0 // indirect
	github.com/pkg/errors v0.9.1
	golang.org/x/net v0.0.0-20201006153459-a7d1128ccaa0 // indirect
	golang.org/x/sys v0.0.0-20201009025420-dfb3f7c4e634 // indirect
)
