//+build wireinject

package wire

import (
	"gitlab.com/iadgarov/rate-limited-ip-location-ex/app"
	"gitlab.com/iadgarov/rate-limited-ip-location-ex/app/drivers/http"
	"gitlab.com/iadgarov/rate-limited-ip-location-ex/app/iplocation"
	"gitlab.com/iadgarov/rate-limited-ip-location-ex/pkg/mmdb"

	"github.com/google/wire"
)

// InitializeAppWithMMDBBackend injects a new App with its dependencies with an mmdb backend
func InitializeAppWithMMDBBackend(path string) (*app.App, error) {
	wire.Build(wire.NewSet(
		app.NewApp,

		http.NewDriver,
		wire.Bind(new(app.Driver), new(*http.Driver)),

		iplocation.NewIPLocationService,
		wire.Bind(new(http.IPLocation), new(*iplocation.Service)),

		mmdb.NewAdapter,
		wire.Bind(new(iplocation.Repository), new(*mmdb.Adapter)),
	))

	return &app.App{}, nil

}
