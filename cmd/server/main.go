package main

import (
	"context"
	"log"
	"os"
	"time"

	"gitlab.com/iadgarov/rate-limited-ip-location-ex/app"
	"gitlab.com/iadgarov/rate-limited-ip-location-ex/wire"
)

const (
	stopTimeout   = 15
	exitCodeError = 1
	exitCodeValid = 0

	envVarDataSource = "DATA_SOURCE"
	dataSourceMMDB   = "mmdb"
)

func main() {
	var err error
	var a *app.App
	switch ds := os.Getenv(envVarDataSource); ds {
	case dataSourceMMDB:
		a, err = wire.InitializeAppWithMMDBBackend("./GeoLite2-City.mmdb")
	default:
		log.Printf("Invalid data source sepcifed (%s) in env var (%s)", ds, envVarDataSource)
		os.Exit(exitCodeError)
	}

	if err != nil {
		log.Println("Failed to initialize app:", err.Error())
		os.Exit(exitCodeError)
	}

	if err := a.Go(); err != nil {
		log.Println("Failed to start app:", err.Error())
		os.Exit(exitCodeError)
	}

	stopCtx, stopCancel := context.WithTimeout(context.Background(), stopTimeout*time.Second)
	if err := a.Stop(stopCtx); err != nil {
		log.Println("Failed to gracefully stop app:", err)
		stopCancel()
		os.Exit(exitCodeError)
	}
	stopCancel()

	os.Exit(exitCodeValid)
}
